var ToDoService = function () {
  this.layDanhSachToDo = function () {
    var promise = axios({
      url: "http://svcy.myclass.vn/api/ToDoList/GetAllTask",
      method: "GET",
    });
    return promise;
  };

  this.xoaToDo = function (maContent) {
    var promise = axios({
      url: `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${maContent}`,
      method: "DELETE",
    });
    return promise;
  };

  this.layThongTinToDo = function (maContent) {
    var promise = axios({
      url: `http://svcy.myclass.vn/api/ToDoList/GetTask?taskName=${maContent}`,
      method: "GET",
    });
    return promise;
  };

  this.doneTask = function (maContent) {
    var promise = axios({
      url: `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${maContent}`,
      method: "PUT",
    });
    return promise;
  };

  this.rejectTask = function (maContent) {
    var promise = axios({
      url: `http://svcy.myclass.vn/api/ToDoList/rejectTask?taskName=${maContent}`,
      method: "PUT",
    });
    return promise;
  };
};
