var Validation = function () {
  this.kiemTraRong = function (value, name, selectorErr) {
    if (value.trim() === "") {
      document.querySelector(
        selectorErr
      ).innerHTML = `${name} không được bỏ trống`;

      return false;
    } else {
      document.querySelector(selectorErr).innerHTML = "";
      return true;
    }
  };
};
