var toDoService = new ToDoService();
var toDay = new Date();
var date =
  toDay.getDate() + "-" + (toDay.getMonth() + 1) + "-" + toDay.getFullYear();

document.getElementById("dateTime").innerHTML = date;

// console.log(toDoService);
var danhSachToDo = function () {
  var promise = toDoService.layDanhSachToDo();

  promise
    .then(function (result) {
      var toDoActive = "";
      var toDoComplete = "";

      for (var i = 0; i < result.data.length; i++) {
        var toDoTask = result.data[i];
        var toDoItem = new ToDo();

        toDoItem.taskName = toDoTask.taskName;
        toDoItem.status = toDoTask.status;
        // console.log(toDoItem);

        if (toDoItem.status === false) {
          toDoActive += `
              <li>
                <span>${toDoItem.taskName}</span>
                <div class="buttons">
                  <button class="remove" onclick="xoaTask('${toDoItem.taskName}')">
                    <i class="fa fa-trash-alt"></i>
                  </button>
                  <button class="complete" onclick="doneTask('${toDoItem.taskName}')">
                    <i class="far fa-check-circle"></i>
                    <i class="fas fa-check-circle"></i>
                  </button>
                </div>
              </li>
          `;
        } else {
          toDoComplete += `
          <li>
            <span>${toDoItem.taskName}</span>
            <div class="buttons">
              <button class="remove" onclick="xoaTask('${toDoItem.taskName}')">
                <i class="fa fa-trash-alt"></i>
              </button>
              <button class="complete" onclick=rejectTask('${toDoItem.taskName}')>
                <i class="far fa-check-circle"></i>
                <i class="fas fa-check-circle"></i>
              </button>
            </div>
          </li>
          `;
        }
      }
      document.getElementById("todo").innerHTML = toDoActive;
      document.getElementById("completed").innerHTML = toDoComplete;
    })
    .catch(function (err) {
      console.log(err.response.data);
    });
};
danhSachToDo();

document.getElementById("addItem").onclick = function () {
  var toDoItem = new ToDo();
  var validate = new Validation();
  var valid = true;
  toDoItem.taskName = document.getElementById("newTask").value;

  valid &= validate.kiemTraRong(toDoItem.taskName, "Task", "#err__kiemTraRong");

  if (!valid) {
    return;
  }

  axios({
    url: "http://svcy.myclass.vn/api/ToDoList/AddTask",
    method: "POST",
    data: toDoItem,
  })
    .then(function (result) {
      console.log("ketqua ->", result.data);
      location.reload();
    })
    .catch(function (err) {
      console.log(err.response.data);
    });
};

var xoaTask = function (maContent) {
  alert(maContent);

  var promise = toDoService.xoaToDo(maContent);
  promise
    .then(function (result) {
      console.log(result.data);
      danhSachToDo();
    })
    .catch(function (err) {
      console.log(err.response.data);
    });
};

var thongTinToDo = function (maContent) {
  var promise = toDoService.layThongTinToDo(maContent);

  promise
    .then(function (result) {
      var thongTinToDo = new ToDo();

      thongTinToDo.taskName = result.data.taskName;
      thongTinToDo.status = result.data.status;

      // var nameTask = result.data.taskName;
      // var statusTask = result.data.status;
      console.log(thongTinToDo);
      return thongTinToDo;
    })
    .catch(function (err) {
      console.log(err.response.data);
    });
};

var doneTask = function (maContent) {
  var promise = toDoService.layThongTinToDo(maContent);

  promise
    .then(function (result) {
      taskDone = result.data;
      console.log(taskDone);
      var promise = toDoService.doneTask(taskDone.taskName);

      promise
        .then(function (result) {
          console.log(result.data);
          danhSachToDo();
        })
        .catch(function (err) {
          console.log(err.result.data);
        });
    })
    .catch(function (err) {
      console.log(err.result.data);
    });
};

var rejectTask = function (maContent) {
  // thongTinToDo(maContent);
  // var taskUpdate = new ToDo();
  var promise = toDoService.layThongTinToDo(maContent);

  promise
    .then(function (result) {
      task = result.data;
      var promise = toDoService.rejectTask(task.taskName);

      promise
        .then(function (result) {
          console.log(result.data);
          danhSachToDo();
        })
        .catch(function (err) {
          console.log(err.result.data);
        });
    })
    .catch(function (err) {
      console.log(err.result.data);
    });
};
